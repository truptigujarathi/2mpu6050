#ifndef TWO_MPU
#define TWO_MPU

#include "Arduino.h"
#include "Wire.h"

/* Include all the global variables */
float accCoef  = 0.02f;
float gyroCoef = 0.98f;
float aC       = 0.02f;
float gC       = 0.98f;
int MPU_add_1  = 0x68;
int MPU_add_2  = 0x69;

#define MPU6050_ADDR         0x68
#define MPU6050_SMPLRT_DIV   0x19
#define MPU6050_CONFIG       0x1a
#define MPU6050_GYRO_CONFIG  0x1b
#define MPU6050_ACCEL_CONFIG 0x1c
#define MPU6050_WHO_AM_I     0x75
#define MPU6050_PWR_MGMT_1   0x6b
#define MPU6050_TEMP_H       0x41
#define MPU6050_TEMP_L       0x42
/* Include all the global variables */



/* Include all the MPU_1 variables*/ 
int16_t rawAccX_1, rawAccY_1, rawAccZ_1, rawTemp_1,
        rawGyroX_1, rawGyroY_1, rawGyroZ_1;

float gyroXoffset_1, gyroYoffset_1, gyroZoffset_1;

float temp_1, accX_1, accY_1, accZ_1, gyroX_1, gyroY_1, gyroZ_1;

float angleGyroX_1, angleGyroY_1, angleGyroZ_1,
      angleAccX_1, angleAccY_1, angleAccZ_1;

float angleX_1, angleY_1, angleZ_1;

float interval_1;
long preInterval_1;
/* Include all the MPU_1 variables */




/* Include all the MPU_2 variables */
int16_t rawAccX_2, rawAccY_2, rawAccZ_2, rawTemp_2,
        rawGyroX_2, rawGyroY_2, rawGyroZ_2;

float gyroXoffset_2, gyroYoffset_2, gyroZoffset_2;

float temp_2, accX_2, accY_2, accZ_2, gyroX_2, gyroY_2, gyroZ_2;

float angleGyroX_2, angleGyroY_2, angleGyroZ_2,
      angleAccX_2, angleAccY_2, angleAccZ_2;

float angleX_2, angleY_2, angleZ_2;

float interval_2;
long preInterval_2;
/* Include all the MPU_2 variables */





/* All the Global Functions */
void writeMPU6050(int MPU_add, byte reg, byte data) {
  Wire.beginTransmission(MPU_add);
  Wire.write(reg);
  Wire.write(data);
  Wire.endTransmission();
}


void readMPU6050(int MPU_add, byte reg) {
  Wire.beginTransmission(MPU_add);
  Wire.write(reg);
  Wire.endTransmission(true);
  Wire.requestFrom(MPU_add, 14);
  byte data =  Wire.read();
  Wire.read();
  return data;
}
/* All the Global Functions */





/* All the Functions For MPU_1 */
void calcGyroOffsets_1(bool console) {
  float x_1 = 0, y_1 = 0, z_1 = 0;
  int16_t rx_1, ry_1, rz_1;

  delay(1000);
  if (console) {
    Serial.println();
	Serial.println("************************************");
	Serial.println("Reading Yaw, Roll and Pitch values from 2-MPU6050 sensors");
    Serial.println("************************************");
    Serial.println("Calculating Gyro offsets for First MPU6050");
  }
  for (int i_1 = 0; i_1 < 3000; i_1++) {
    if (console && i_1 % 1000 == 0) {
      Serial.print(".");
    }
    Wire.beginTransmission(MPU_add_1);
    Wire.write(0x3B);
    Wire.endTransmission(false);
    Wire.requestFrom(MPU_add_1, 14, (int)true);

    Wire.read() << 8 | Wire.read();
    Wire.read() << 8 | Wire.read();
    Wire.read() << 8 | Wire.read();
    Wire.read() << 8 | Wire.read();
    rx_1 = Wire.read() << 8 | Wire.read();
    ry_1 = Wire.read() << 8 | Wire.read();
    rz_1 = Wire.read() << 8 | Wire.read();

    x_1 += ((float)rx_1) / 65.5;
    y_1 += ((float)ry_1) / 65.5;
    z_1 += ((float)rz_1) / 65.5;
  }
  gyroXoffset_1 = x_1 / 3000;
  gyroYoffset_1 = y_1 / 3000;
  gyroZoffset_1 = z_1 / 3000;

  if (console) {
    Serial.println();
    Serial.println("Done!!!");
    Serial.print("X_1 : "); Serial.println(gyroXoffset_1);
    Serial.print("Y_1 : "); Serial.println(gyroYoffset_1);
    Serial.print("Z_1 : "); Serial.println(gyroZoffset_1);
    Serial.print("************************************");
    delay(30);
  }
}


void MPU_1_update() {
  Wire.beginTransmission(MPU_add_1);
  Wire.write(0x3B);
  Wire.endTransmission(false);
  Wire.requestFrom(MPU_add_1, 14);

  rawAccX_1 = Wire.read() << 8 | Wire.read();
  rawAccY_1 = Wire.read() << 8 | Wire.read();
  rawAccZ_1 = Wire.read() << 8 | Wire.read();
  rawTemp_1 = Wire.read() << 8 | Wire.read();
  rawGyroX_1 = Wire.read() << 8 | Wire.read();
  rawGyroY_1 = Wire.read() << 8 | Wire.read();
  rawGyroZ_1 = Wire.read() << 8 | Wire.read();

  temp_1 = (rawTemp_1 + 12412.0) / 340.0;

  accX_1 = ((float)rawAccX_1) / 16384.0;
  accY_1 = ((float)rawAccY_1) / 16384.0;
  accZ_1 = ((float)rawAccZ_1) / 16384.0;

  angleAccX_1 = atan2(accY_1, accZ_1 + abs(accX_1)) * 360 / 2.0 / PI;
  angleAccY_1 = atan2(accX_1, accZ_1 + abs(accY_1)) * 360 / -2.0 / PI;

  gyroX_1 = ((float)rawGyroX_1) / 65.5;
  gyroY_1 = ((float)rawGyroY_1) / 65.5;
  gyroZ_1 = ((float)rawGyroZ_1) / 65.5;

  gyroX_1 -= gyroXoffset_1;
  gyroY_1 -= gyroYoffset_1;
  gyroZ_1 -= gyroZoffset_1;

  interval_1 = (millis() - preInterval_1) * 0.001;

  angleGyroX_1 += gyroX_1 * interval_1;
  angleGyroY_1 += gyroY_1 * interval_1;
  angleGyroZ_1 += gyroZ_1 * interval_1;

  angleX_1 = (gyroCoef * (angleX_1 + gyroX_1 * interval_1)) + (accCoef * angleAccX_1);
  angleY_1 = (gyroCoef * (angleY_1 + gyroY_1 * interval_1)) + (accCoef * angleAccY_1);
  angleZ_1 = angleGyroZ_1;

  preInterval_1 = millis();

  return angleAccX_1;
  return angleAccY_1;
  return angleAccZ_1;



}



void MPU_1_begin() {
  writeMPU6050(MPU_add_1, MPU6050_SMPLRT_DIV, 0x00);
  writeMPU6050(MPU_add_1, MPU6050_CONFIG, 0x00);
  writeMPU6050(MPU_add_1, MPU6050_GYRO_CONFIG, 0x08);
  writeMPU6050(MPU_add_1, MPU6050_ACCEL_CONFIG, 0x00);
  writeMPU6050(MPU_add_1, MPU6050_PWR_MGMT_1, 0x01);
  MPU_1_update();
  angleGyroX_1 = 0;
  angleGyroY_1 = 0;
  angleX_1 = angleAccX_1;
  angleY_1 = angleAccY_1;
  preInterval_1 = millis();
}
/* All the Functions For MPU_1 */





/* All the Functions For MPU_2 */
void calcGyroOffsets_2(bool console) {
  float x_2 = 0, y_2 = 0, z_2 = 0;
  int16_t rx_2, ry_2, rz_2;

  delay(1000);
  if (console) {
    Serial.println();
    Serial.println("************************************");
    Serial.println("Calculating Gyro Offsets for second MPU6050");
  }
  for (int i_2 = 0; i_2 < 3000; i_2++) {
    if (console && i_2 % 1000 == 0) {
      Serial.print(".");
    }
    Wire.beginTransmission(MPU_add_2);
    Wire.write(0x3B);
    Wire.endTransmission(false);
    Wire.requestFrom(MPU_add_2, 14, (int)true);

    Wire.read() << 8 | Wire.read();
    Wire.read() << 8 | Wire.read();
    Wire.read() << 8 | Wire.read();
    Wire.read() << 8 | Wire.read();
    rx_2 = Wire.read() << 8 | Wire.read();
    ry_2 = Wire.read() << 8 | Wire.read();
    rz_2 = Wire.read() << 8 | Wire.read();

    x_2 += ((float)rx_2) / 65.5;
    y_2 += ((float)ry_2) / 65.5;
    z_2 += ((float)rz_2) / 65.5;
  }
  gyroXoffset_2 = x_2 / 3000;
  gyroYoffset_2 = y_2 / 3000;
  gyroZoffset_2 = z_2 / 3000;

  if (console) {
    Serial.println();
    Serial.println("Done!!!");
    Serial.print("X_2 : "); Serial.println(gyroXoffset_2);
    Serial.print("Y_2 : "); Serial.println(gyroYoffset_2);
    Serial.print("Z_2 : "); Serial.println(gyroZoffset_2);
    Serial.println("Program will start after 2 seconds");
    Serial.print("************************************\n");
    delay(2000);
  }
}


void MPU_2_update() {
  Wire.beginTransmission(MPU_add_2);
  Wire.write(0x3B);
  Wire.endTransmission(false);
  Wire.requestFrom(MPU_add_2, 14, (int)true);

  rawAccX_2 = Wire.read() << 8 | Wire.read();
  rawAccY_2 = Wire.read() << 8 | Wire.read();
  rawAccZ_2 = Wire.read() << 8 | Wire.read();
  rawTemp_2 = Wire.read() << 8 | Wire.read();
  rawGyroX_2 = Wire.read() << 8 | Wire.read();
  rawGyroY_2 = Wire.read() << 8 | Wire.read();
  rawGyroZ_2 = Wire.read() << 8 | Wire.read();

  temp_2 = (rawTemp_2 + 12412.0) / 340.0;

  accX_2 = ((float)rawAccX_2) / 16384.0;
  accY_2 = ((float)rawAccY_2) / 16384.0;
  accZ_2 = ((float)rawAccZ_2) / 16384.0;

  angleAccX_2 = atan2(accY_2, accZ_2 + abs(accX_2)) * 360 / 2.0 / PI;
  angleAccY_2 = atan2(accX_2, accZ_2 + abs(accY_2)) * 360 / -2.0 / PI;

  gyroX_2 = ((float)rawGyroX_2) / 65.5;
  gyroY_2 = ((float)rawGyroY_2) / 65.5;
  gyroZ_2 = ((float)rawGyroZ_2) / 65.5;

  gyroX_2 -= gyroXoffset_2;
  gyroY_2 -= gyroYoffset_2;
  gyroZ_2 -= gyroZoffset_2;

  interval_2 = (millis() - preInterval_2) * 0.001;

  angleGyroX_2 += gyroX_2 * interval_2;
  angleGyroY_2 += gyroY_2 * interval_2;
  angleGyroZ_2 += gyroZ_2 * interval_2;

  angleX_2 = (gyroCoef * (angleX_2 + gyroX_2 * interval_2)) + (accCoef * angleAccX_2);
  angleY_2 = (gyroCoef * (angleY_2 + gyroY_2 * interval_2)) + (accCoef * angleAccY_2);
  angleZ_2 = angleGyroZ_2;

  preInterval_2 = millis();

  return angleAccX_2;
  return angleAccY_2;
  return angleAccZ_2;

}

void MPU_2_begin() {
  writeMPU6050(MPU_add_2, MPU6050_SMPLRT_DIV, 0x00);
  writeMPU6050(MPU_add_2, MPU6050_CONFIG, 0x00);
  writeMPU6050(MPU_add_2, MPU6050_GYRO_CONFIG, 0x08);
  writeMPU6050(MPU_add_2, MPU6050_ACCEL_CONFIG, 0x00);
  writeMPU6050(MPU_add_2, MPU6050_PWR_MGMT_1, 0x01);
  MPU_2_update();
  angleGyroX_2 = 0;
  angleGyroY_2 = 0;
  angleX_2 = angleAccX_2;
  angleY_2 = angleAccY_2;
  preInterval_2 = millis();
}
/* All the Functions For MPU_2 */


#endif 
