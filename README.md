# Simultaneous Data Acquisition Through 2 MPU6050 Sensors

This is a pure **C** code made to run on Arduino boards for Simultaneous Acquisition of Angular Data from two I2C based Sensors.

Data from **MPU6050** is RAW data and it needs to be processed before we can use it. There are many codes which give raw data. 

The codes which contain the the required data are typically made to only use a single **MPU6050** sensor. 

This code is customized specially to be used with our GAIT Cycle analysis tool. 

We make use of two MPU6050 sensors and the angluar data ie, **YAW**, **ROLL**, **PITCH** **(X,Y,Z)** are captured simultaneously using I2C

communication protocol.

The sensors have two default communication addresses: 0x68  and 0x69
\
Sensors are connected as per the configuration below:
	      
                 
     
| MPU6050/GY-521| Arduino UNO Pin|
| ------ | ------ |
| VCC | 5V (the GY-521 has a voltage regulator) |
| GND | GND | 
|SDA | A4 (I2C SDA) |
| SCL | A5 (I2C SCL)|
                 