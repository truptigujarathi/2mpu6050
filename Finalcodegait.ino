#include <SimpleTimer.h>
#include "TWO_MPU.h"        //include the header file containing all the functions and variables
#include <Wire.h>                 //include the library for initializing the I2c communication


// the timer object
SimpleTimer timer;
float s,m;
// a function to be executed periodically
void repeatMe() {
    s=millis();
    m=s/1000;
    
    Serial.print("t");
    Serial.print(m);
    
    Serial.print("\t");
}

void setup() {
    Serial.begin(9600);
    timer.setInterval(1, repeatMe);
    
    Wire.begin();                     //Start the communication
    MPU_1_begin();                    //Start the First MPU
    //calcGyroOffsets_1(true);        //Calculate offset for First MPU
   //de delay(1);                         //Delay
    MPU_2_begin();                    //Start the Second MPU
    //calcGyroOffsets_2(true);        //Calculate offset for Second MPU
}

void loop() {
    timer.run();  
    
   MPU_1_update();                   //Update the First MPU
   //Serial.print(angleX_1);           //Angle X of First MPU 
   //Serial.print("\t");
   Serial.print(angleY_1);         //Angle Y of First MPU
   Serial.print("\t");
   //Serial.print(angleZ_1);       //Angle Z of First MPU
   //Serial.print("\t");
    
    MPU_2_update();                   //Update the Second MPU
    //Serial.print(angleX_2);         //Angle X of Second MPU
   // Serial.print("\t");
    Serial.println(angleY_2);         //Angle Y of Second MPU
   // Serial.print("\t");
    //Serial.println(angleZ_2);       //Angle Z of Second MPU
  
}
